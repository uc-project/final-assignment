# Final Assignment: Flight Delay Predictor

## Introduction
This repository is developed for a travel booking website aiming to enhance the customer experience for delayed flights. We wish to offer a service that informs customers about the probability of a flight delay based on weather conditions before they make a booking, especially for the busiest airports in the US.

Using the provided dataset on the on-time performance of domestic flights operated by major air carriers, our goal is to employ machine learning to ascertain the likelihood of flight delays based on available weather data.


## Data
### Dataset Overview
This data is sourced from the Office of Airline Information, Bureau of Transportation Statistics (BTS) and spans flights between 2014 and 2018. It includes details such as date, time, origin, destination, airline, distance, and delay status.

You can access and download the dataset from https://ucstaff-my.sharepoint.com/personal/ibrahim_radwan_canberra_edu_au/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fibrahim%5Fradwan%5Fcanberra%5Fedu%5Fau%2FDocuments%2Fteaching%5Fjunction%2F2021%2Fs2%2Fdsts%2Ffinal%5Fproject%2Fdata%5Fcompressed&ga=1


## Repository Contents
### onpremises.ipynb:
This Jupyter Notebook file contains the code that can be run locally once the environment is set up. It produces a report comparing the predictions of two datasets on the same logistic regression classification model.

### oncloud.ipynb:
This notebook is tailored for execution in AWS SageMaker. It evaluates two datasets using the Linear learner and XGBoost Model, offering insights into their performance.



## Getting Started

### Local Setup:
Make sure you have Jupyter Notebook installed (Python 3.8 or above). If not, install Jupyter:

```bash
pip install notebook

pip install -r requirements.txt

jupyter notebook

### AWS SageMaker Setup:
S3 Bucket Creation:
    Prior to running the oncloud.ipynb, create an S3 bucket on AWS and upload the source data.

Permissions:
    Ensure you have the necessary permissions to run SageMaker and associated services on AWS.

Executing the Code:
    Open oncloud.ipynb within SageMaker.
    Run the code cells. Note: Package installations are integrated within the notebook for SageMaker.

### Expected Outcomes
Users can expect a comprehensive report upon executing the code. This report will delve into the predictive performance of the datasets, assisting customers in making well-informed decisions about their flight bookings, especially concerning potential delays.

